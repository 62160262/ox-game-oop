/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ox.oop;

/**
 *
 * @author Acer
 */
class Table {

    char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    Player playerX;
    Player playerO;
    Player currentPlayer;
    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;

    }

    public void showTable() {
        System.out.println("1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + "");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println(" ");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            return true;
        }
        return false;
    }
}
