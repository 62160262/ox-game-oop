/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ox.oop;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row,col;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);

    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    static void input() {
        //Normal flow
        Scanner kb = new Scanner (System.in);
        while (true) {
            System.out.println("Please input Row Col :");
            int row =kb.nextInt() - 1;
            int col =kb.nextInt() - 1;
           if(!table.setRowCol(row, col)){
               break
           } else {
               ;
            } 
            
            
            System.out.println("Error : table row and col is not empty!!!");
        }
    }

    public void run() {
        this.showWelcome();
        this.showTable();
        this.input();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Game game = new Game();
        game.run();

    }

}
